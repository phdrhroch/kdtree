#ifndef CAMERA_H
#define CAMERA_H

#include <QMatrix4x4>
#include <QVector2D>
#include <QVector3D>
#include <QVector4D>

#include "trackball.h"

class QVector3D;

class Camera
{
public:
    Camera(const QVector3D& eye, const QVector3D& center, const QVector3D& up, float fov, int w, int h);
    void apply();
    void resize(int w, int h);
    void zoom(int delta);
    void movespush(int x, int y);
    void moverelease(int x, int y);
    void arcballpush(int x, int y);
    void arcballrelease(int x, int y);

private:
    QMatrix4x4 mProjection;
    QMatrix4x4 mModelView;

    int mW;
    int mH;
    float mFov;

    QVector2D mMoveCoords;
    TrackBall* mTrackBall;
};

#endif // CAMERA_H
