#include "qglbuffercuda.h"
#include "globaldefs.h"

#include <cuda_gl_interop.h>

QGLBufferCuda::QGLBufferCuda()
{

}

void QGLBufferCuda::registerCuda()
{
    cudaGLRegisterBufferObject(bufferId());
    cudaGLSetBufferObjectMapFlags(bufferId(), cudaGLMapFlagsNone);
    gpuErrchk();
}

void QGLBufferCuda::unregisterCuda()
{
    cudaGLUnregisterBufferObject(bufferId());
    gpuErrchk();
}

void* QGLBufferCuda::mapCuda()
{
    void* devptr;
    cudaGLMapBufferObject(&devptr, bufferId());
    gpuErrchk();
    return devptr;
}

void QGLBufferCuda::unmapCuda()
{
    cudaGLUnmapBufferObject(bufferId());
    gpuErrchk();
}
