#include "kdtreecomputenormal.cuh"

__constant__ float inv3 = 0.33333333333f;
__constant__ float root3 = 1.73205080757f;
__constant__ float root32 = 1.25992104989f;

__constant__ float PI = 3.141592654f;

#include <math.h>

#ifdef MAX
#undef MAX
#endif

#define MAX(a, b) ((a)>(b)?(a):(b))

#define n 3

__device__
float hypot2(float x, float y) {
  return sqrt(x*x+y*y);
}

// Symmetric Householder reduction to tridiagonal form.

__device__
void tred2(Matrix3x3& V, VBOVertex& d, VBOVertex& e) {

//  This is derived from the Algol procedures tred2 by
//  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
//  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
//  Fortran subroutine in EISPACK.

  for (int j = 0; j < n; j++) {
    d[j] = V(n-1,j);
  }

  // Householder reduction to tridiagonal form.

  for (int i = n-1; i > 0; i--) {

    // Scale to avoid under/overflow.

    float scale = 0.0f;
    float h = 0.0f;
    for (int k = 0; k < i; k++) {
      scale = scale + fabs(d[k]);
    }
    if (scale == 0.0f) {
      e[i] = d[i-1];
      for (int j = 0; j < i; j++) {
        d[j] = V(i-1,j);
        V(i,j) = 0.0f;
        V(j,i) = 0.0f;
      }
    } else {

      // Generate Householder vector.

      for (int k = 0; k < i; k++) {
        d[k] /= scale;
        h += d[k] * d[k];
      }
      float f = d[i-1];
      float g = sqrt(h);
      if (f > 0) {
        g = -g;
      }
      e[i] = scale * g;
      h = h - f * g;
      d[i-1] = f - g;
      for (int j = 0; j < i; j++) {
        e[j] = 0.0f;
      }

      // Apply similarity transformation to remaining columns.

      for (int j = 0; j < i; j++) {
        f = d[j];
        V(j,i) = f;
        g = e[j] + V(j,j) * f;
        for (int k = j+1; k <= i-1; k++) {
          g += V(k,j) * d[k];
          e[k] += V(k,j) * f;
        }
        e[j] = g;
      }
      f = 0.0f;
      for (int j = 0; j < i; j++) {
        e[j] /= h;
        f += e[j] * d[j];
      }
      float hh = f / (h + h);
      for (int j = 0; j < i; j++) {
        e[j] -= hh * d[j];
      }
      for (int j = 0; j < i; j++) {
        f = d[j];
        g = e[j];
        for (int k = j; k <= i-1; k++) {
          V(k,j) -= (f * e[k] + g * d[k]);
        }
        d[j] = V(i-1,j);
        V(i,j) = 0.0f;
      }
    }
    d[i] = h;
  }

  // Accumulate transformations.

  for (int i = 0; i < n-1; i++) {
    V(n-1,i) = V(i,i);
    V(i,i) = 1.0f;
    float h = d[i+1];
    if (h != 0.0f) {
      for (int k = 0; k <= i; k++) {
        d[k] = V(k,i+1) / h;
      }
      for (int j = 0; j <= i; j++) {
        float g = 0.0f;
        for (int k = 0; k <= i; k++) {
          g += V(k,i+1) * V(k,j);
        }
        for (int k = 0; k <= i; k++) {
          V(k,j) -= g * d[k];
        }
      }
    }
    for (int k = 0; k <= i; k++) {
      V(k,i+1) = 0.0f;
    }
  }
  for (int j = 0; j < n; j++) {
    d[j] = V(n-1,j);
    V(n-1,j) = 0.0f;
  }
  V(n-1,n-1) = 1.0f;
  e[0] = 0.0f;
}

// Symmetric tridiagonal QL algorithm.

__device__
void tql2(Matrix3x3& V, VBOVertex& d, VBOVertex& e) {

//  This is derived from the Algol procedures tql2, by
//  Bowdler, Martin, Reinsch, and Wilkinson, Handbook for
//  Auto. Comp., Vol.ii-Linear Algebra, and the corresponding
//  Fortran subroutine in EISPACK.

  for (int i = 1; i < n; i++) {
    e[i-1] = e[i];
  }
  e[n-1] = 0.0f;

  float f = 0.0f;
  float tst1 = 0.0f;
  float eps = pow(2.0f,-52.0f);
  for (int l = 0; l < n; l++) {

    // Find small subdiagonal element

    tst1 = MAX(tst1,fabs(d[l]) + fabs(e[l]));
    int m = l;
    while (m < n) {
      if (fabs(e[m]) <= eps*tst1) {
        break;
      }
      m++;
    }

    // If m == l, d[l] is an eigenvalue,
    // otherwise, iterate.

    if (m > l) {
      int iter = 0;
      do {
        iter = iter + 1;  // (Could check iteration count here.)

        // Compute implicit shift

        float g = d[l];
        float p = (d[l+1] - g) / (2.0f * e[l]);
        float r = hypot2(p,1.0f);
        if (p < 0) {
          r = -r;
        }
        d[l] = e[l] / (p + r);
        d[l+1] = e[l] * (p + r);
        float dl1 = d[l+1];
        float h = g - d[l];
        for (int i = l+2; i < n; i++) {
          d[i] -= h;
        }
        f = f + h;

        // Implicit QL transformation.

        p = d[m];
        float c = 1.0f;
        float c2 = c;
        float c3 = c;
        float el1 = e[l+1];
        float s = 0.0;
        float s2 = 0.0;
        for (int i = m-1; i >= l; i--) {
          c3 = c2;
          c2 = c;
          s2 = s;
          g = c * e[i];
          h = c * p;
          r = hypot2(p,e[i]);
          e[i+1] = s * r;
          s = e[i] / r;
          c = p / r;
          p = c * d[i] - s * g;
          d[i+1] = h + s * (c * g + s * d[i]);

          // Accumulate transformation.

          for (int k = 0; k < n; k++) {
            h = V(k,i+1);
            V(k,i+1) = s * V(k,i) + c * h;
            V(k,i) = c * V(k,i) - s * h;
          }
        }
        p = -s * s2 * c3 * el1 * e[l] / dl1;
        e[l] = s * p;
        d[l] = c * p;

        // Check for convergence.

      } while (fabs(e[l]) > eps*tst1);
    }
    d[l] = d[l] + f;
    e[l] = 0.0f;
  }

  // Sort eigenvalues and corresponding vectors.

  for (int i = 0; i < n-1; i++) {
    int k = i;
    float p = d[i];
    for (int j = i+1; j < n; j++) {
      if (d[j] < p) {
        k = j;
        p = d[j];
      }
    }
    if (k != i) {
      d[k] = d[i];
      d[i] = p;
      for (int j = 0; j < n; j++) {
        p = V(j,i);
        V(j,i) = V(j,k);
        V(j,k) = p;
      }
    }
  }
}

__device__
void eigen_decomposition(const Matrix3x3& A, Matrix3x3& V, VBOVertex& d)
{
  VBOVertex e;
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      V[i][j] = A[i][j];
    }
  }
  tred2(V, d, e);
  tql2(V, d, e);
}

__device__
void computeRoots (const Matrix3x3& matrix, float* root)
{
    float a00 = (float)matrix(0,0);
    float a01 = (float)matrix(0,1);
    float a02 = (float)matrix(0,2);
    float a11 = (float)matrix(1,1);
    float a12 = (float)matrix(1,2);
    float a22 = (float)matrix(2,2);
    float c0 = a00*a11*a22 + 2.0f*a01*a02*a12 - a00*a12*a12 - a11*a02*a02 - a22*a01*a01;
    float c1 = a00*a11 - a01*a01 + a00*a22 - a02*a02 + a11*a22 - a12*a12;
    float c2 = a00 + a11 + a22;
    float c2Div3 = c2*inv3;
    float aDiv3 = (c1 - c2*c2Div3)*inv3;
    if (aDiv3 > 0.0) { aDiv3 = 0.0; }
    float mbDiv2 = 0.5f*(c0 + c2Div3*(2.0f*c2Div3*c2Div3 - c1));
    float q = mbDiv2*mbDiv2 + aDiv3*aDiv3*aDiv3;
    if (q > 0.0) { q = 0.0; }
    float magnitude = sqrt(-aDiv3);
    float angle = atan2(sqrt(-q),mbDiv2)*inv3;
    float cs = cos(angle);
    float sn = sin(angle);
    root[0] = c2Div3 + 2.0f*magnitude*cs;
    root[1] = c2Div3 - magnitude*(cs + root3*sn);
    root[2] = c2Div3 - magnitude*(cs - root3*sn);
    // Sort the roots here to obtain root[0] <= root[1] <= root[2].
}

__device__
float mineigenvalue(const Matrix3x3& matrix)
{
    // p1 = A(1,2)^2 + A(1,3)^2 + A(2,3)^2
    float p1 = matrix(0,1)*matrix(0,1) + matrix(0,2)*matrix(0,2) + matrix(1,2)*matrix(1,2);

    if (p1 <= 1e-6)
    {
        if (matrix(0,0) < matrix(1,1))
        {
            return (matrix(2,2) < matrix(0,0) ? matrix(2,2) : matrix(0,0));
        }
        else
        {
            return (matrix(2,2) < matrix(1,1) ? matrix(2,2) : matrix(1,1));
        }
    }
    else
    {
        float q = (matrix(0,0) + matrix(1,1) + matrix(2,2)) / 3.0f; //trace(A)/3
        float p2 = p2 = (matrix(0,0) - q)*(matrix(0,0) - q) + (matrix(1,1) - q)*(matrix(1,1) - q) + (matrix(2,2) - q)*(matrix(2,2) - q) + 2.0f * p1;
        float p = sqrtf(p2 / 6.0f);


        Matrix3x3 b(matrix);
        //B = (1 / p) * (A - q * I)       % I is the identity matrix
        b(0,0) -= q;
        b(1,1) -= q;
        b(2,2) -= q;

        float div = 1.0f / p;
        b *= div;

        float r = b.determinant() / 2.0f;

        // % In exact arithmetic for a symmetric matrix  -1 <= r <= 1
        // % but computation error can leave it slightly outside this range.
        float phi;
        if (r <= -1.0f)
          phi = PI / 3.0f;
        else if (r >= 1.0f)
          phi = 0.0f;
        else
          phi = acosf(r) / 3.0;


        // % the eigenvalues satisfy eig3 <= eig2 <= eig1
        //float eig1 = q + 2 * p * cos(phi);
        float eig3 = q - p * ( cosf(phi) + root3*sinf(phi) );
        //float eig2 = 3 * q - eig1 - eig3;     //% since trace(A) = eig1 + eig2 + eig3
        return eig3;
    }
}

__global__
void Cuda_KDTreeComputeNormal(VBOVertex* normals, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index)
{
    // get index of thread in grid
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // check if thread is valid
    if (idx >= treeSize) return;

/*
    VBOVertex first = tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + 0 * blockDim.x]];
    VBOVertex second = tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + 1 * blockDim.x]];
    VBOVertex third = tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + 2 * blockDim.x]];

    VBOVertex result =  crossProduct(second - first, third - first);
    result.normalize();
    normals[index[idx]] = result;

    return;
*/

    VBOVertex mean; mean.x = 0.0f; mean.y = 0.0f; mean.z = 0.0f;
    for (int i = 0; i < k; ++i)
    {
        mean += tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + i * blockDim.x]];
    }
    mean /= k;

    Matrix3x3 matrix;

    for (int i = 0; i < k; ++i)
    {
        for (int x = 0; x < 3; x++)
        for (int y = 0; y < 3; y++) {
            matrix(x,y) += (mean[x] - tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + i * blockDim.x]][x]) * (mean[y] - tree[neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + i * blockDim.x]][y]);
        }
    }

    float min_eigenvalue = mineigenvalue(matrix);

    matrix(0,0) -= min_eigenvalue;
    matrix(1,1) -= min_eigenvalue;
    matrix(2,2) -= min_eigenvalue;

    VBOVertex vec1 = crossProduct(matrix[0], matrix[1]);
    VBOVertex vec2 = crossProduct(matrix[0], matrix[2]);
    VBOVertex vec3 = crossProduct(matrix[1], matrix[2]);

    float len1 = vec1.length2();
    float len2 = vec2.length2();
    float len3 = vec3.length2();

    VBOVertex normal;

    if (len1 >= len2 && len1 >= len3)
      normal = vec1 / sqrt(len1);
    else if (len2 >= len1 && len2 >= len3)
      normal = vec2 / sqrt(len2);
    else
      normal = vec3 / sqrt(len3);

    //Matrix3x3 eigenvectors;
    //VBOVertex eigenvalues;
    //eigen_decomposition(matrix, eigenvectors, eigenvalues);
    /*
    float mineigen = mineigenvalue(matrix);
    matrix(0,0) -= mineigen;
    matrix(1,1) -= mineigen;
    matrix(2,2) -= mineigen;
    */

    /*
    VBOVertex normal;
    if (matrix(0,0) < matrix(1,1))
    {
        if (matrix(0, 0) < matrix(2,2)) normal = crossProduct(matrix[1], matrix[2]);
        else normal = crossProduct(matrix[0], matrix[1]);
    }
    else
    {
        if (matrix(1, 1) < matrix(2,2)) normal = crossProduct(matrix[0], matrix[2]);
        else normal = crossProduct(matrix[0], matrix[1]);
    }
    */

    /*
    VBOVertex normal;
    normal.x = matrix(0,0);
    normal.y = matrix(1,0);
    normal.z = matrix(2,0);
    */


    //VBOVertex normal; normal.x = eigenvectors(2, 0); normal.y = eigenvectors(2, 1); normal.z = eigenvectors(2, 2);
    //VBOVertex normal = crossProduct(matrix[0], matrix[2]);
    //normal.normalize();

    normals[index[idx]] = normal;
    //normals[idx] = normal;
}

void C_KDTreeComputeNormal(const KernelRunData& data, VBOVertex* normals, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index)
{
    Cuda_KDTreeComputeNormal<<<data.dimGrid, data.dimBlock>>>(normals, neightbours, tree, treeSize, k, index);
}
