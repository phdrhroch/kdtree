#ifndef VBOVERTEX_H
#define VBOVERTEX_H

#include <vector>
#include <math.h>
#include <ostream>
#include <istream>

#include <cuda_runtime.h>

struct VBOVertex
{
    __host__ __device__ VBOVertex() {}
    __host__ __device__ VBOVertex(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }
    __host__ __device__ VBOVertex(float4 point) { x = point.x; y = point.y; z = point.z; }

    union
    {
        struct
        {
            float x;
            float y;
            float z;
        };
        float data[3];

    };
    float padding;

    __host__ __device__ VBOVertex operator +(float f) const;
    __host__ __device__ VBOVertex operator -(float f) const;
    __host__ __device__ VBOVertex operator *(float f) const;
    __host__ __device__ VBOVertex operator /(float f) const;

    __host__ __device__ VBOVertex& operator +=(float f);
    __host__ __device__ VBOVertex& operator -=(float f);
    __host__ __device__ VBOVertex& operator *=(float f);
    __host__ __device__ VBOVertex& operator /=(float f);

    __host__ __device__ VBOVertex operator +(const VBOVertex& v) const;
    __host__ __device__ VBOVertex operator -(const VBOVertex& v) const;
    __host__ __device__ VBOVertex operator *(const VBOVertex& v) const;
    __host__ __device__ VBOVertex operator /(const VBOVertex& v) const;

    __host__ __device__ VBOVertex& operator +=(const VBOVertex& v);
    __host__ __device__ VBOVertex& operator -=(const VBOVertex& v);
    __host__ __device__ VBOVertex& operator *=(const VBOVertex& v);
    __host__ __device__ VBOVertex& operator /=(const VBOVertex& v);

    __host__ __device__ float operator[](int i) const;
    __host__ __device__ float& operator[](int i);

    __host__ __device__
    void normalize();

    __host__ __device__
    float length() const;

     __host__ __device__
     float length2() const;

    __host__ __device__
    friend float distanceSquared(const VBOVertex& p1, const VBOVertex& p2);

    friend std::ostream& operator<<(std::ostream& out, const VBOVertex& p);
    friend std::istream& operator<<(std::istream& in, VBOVertex& p);
    friend VBOVertex crossProduct(const VBOVertex& p1, const VBOVertex& p2);


    /*inline Eigen::Map<Eigen::Vector3f> getVector3fMap () { return (Eigen::Vector3f::Map (data)); } \
    inline const Eigen::Map<const Eigen::Vector3f> getVector3fMap () const { return (Eigen::Vector3f::Map (data)); } \
    inline Eigen::Map<Eigen::Vector4f, Eigen::Aligned> getVector4fMap () { return (Eigen::Vector4f::MapAligned (data)); } \
    inline const Eigen::Map<const Eigen::Vector4f, Eigen::Aligned> getVector4fMap () const { return (Eigen::Vector4f::MapAligned (data)); } \
    inline Eigen::Map<Eigen::Array3f> getArray3fMap () { return (Eigen::Array3f::Map (data)); } \
    inline const Eigen::Map<const Eigen::Array3f> getArray3fMap () const { return (Eigen::Array3f::Map (data)); } \
    inline Eigen::Map<Eigen::Array4f, Eigen::Aligned> getArray4fMap () { return (Eigen::Array4f::MapAligned (data)); } \
    inline const Eigen::Map<const Eigen::Array4f, Eigen::Aligned> getArray4fMap () const { return (Eigen::Array4f::MapAligned (data)); }*/
};

__host__ __device__
inline float distanceSquared(const VBOVertex& p1, const VBOVertex& p2)
{
    float dx = p1.x - p2.x;
    float dy = p1.y - p2.y;
    float dz = p1.z - p2.z;

    return dx*dx + dy*dy + dz*dz;
}

__host__ __device__
inline VBOVertex crossProduct(const VBOVertex& p1, const VBOVertex& p2)
{
    VBOVertex result;
    result.x = p1.y*p2.z - p1.z*p2.y;
    result.y = p1.z*p2.x - p1.x*p2.z;
    result.z = p1.x*p2.y - p1.y*p2.x;

    return result;
}

__host__ __device__
inline VBOVertex crossProduct(float* p1, float* p2)
{
    VBOVertex result;
    result.x = p1[1]*p2[2] - p1[2]*p2[1];
    result.y = p1[2]*p2[0] - p1[0]*p2[2];
    result.z = p1[0]*p2[1] - p1[1]*p2[0];

    return result;
}

__host__ __device__
inline float VBOVertex::length2() const
{
    VBOVertex temp(*this);
    temp *= temp;
    return temp.x + temp.y + temp.z;
}

#ifndef _WIN32
#include <stdint.h>
typedef uint64_t OCTCODE;
#else
typedef __int64  OCTCODE;
#endif

typedef std::vector<VBOVertex> Vertices;

typedef std::vector<OCTCODE> Octcodes;

#define MAX_LEVEL 20

inline VBOVertex VBOVertex::operator +(float f) const
{
    VBOVertex result(*this);
    result.x += f;
    result.y += f;
    result.z += f;
    return result;
}

inline VBOVertex VBOVertex::operator -(float f) const
{
    VBOVertex result(*this);
    result.x -= f;
    result.y -= f;
    result.z -= f;
    return result;
}

inline VBOVertex VBOVertex::operator *(float f) const
{
    VBOVertex result(*this);
    result.x *= f;
    result.y *= f;
    result.z *= f;
    return result;
}

inline VBOVertex VBOVertex::operator /(float f) const
{
    VBOVertex result(*this);
    result.x /= f;
    result.y /= f;
    result.z /= f;
    return result;
}

inline VBOVertex& VBOVertex::operator +=(float f)
{
    x += f;
    y += f;
    z += f;
    return *this;
}

inline VBOVertex& VBOVertex::operator -=(float f)
{
    x -= f;
    y -= f;
    z -= f;
    return *this;
}

inline VBOVertex& VBOVertex::operator *=(float f)
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

inline VBOVertex& VBOVertex::operator /=(float f)
{
    x /= f;
    y /= f;
    z /= f;
    return *this;
}

inline VBOVertex VBOVertex::operator +(const VBOVertex& v) const
{
    VBOVertex result(*this);
    result.x += v.x;
    result.y += v.y;
    result.z += v.z;
    return result;
}

inline VBOVertex VBOVertex::operator -(const VBOVertex& v) const
{
    VBOVertex result(*this);
    result.x -= v.x;
    result.y -= v.y;
    result.z -= v.z;
    return result;
}

inline VBOVertex VBOVertex::operator *(const VBOVertex& v) const
{
    VBOVertex result(*this);
    result.x *= v.x;
    result.y *= v.y;
    result.z *= v.z;
    return result;
}

inline VBOVertex VBOVertex::operator /(const VBOVertex& v) const
{
    VBOVertex result(*this);
    result.x /= v.x;
    result.y /= v.y;
    result.z /= v.z;
    return result;
}


inline VBOVertex& VBOVertex::operator +=(const VBOVertex& v)
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

inline VBOVertex& VBOVertex::operator -=(const VBOVertex& v)
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

inline VBOVertex& VBOVertex::operator *=(const VBOVertex& v)
{
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
}

inline VBOVertex& VBOVertex::operator /=(const VBOVertex& v)
{
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
}

inline float VBOVertex::operator[](int i) const
{
    return data[i];
}

inline float& VBOVertex::operator[](int i)
{
    return data[i];
}

inline void VBOVertex::normalize()
{
    *this /= length();
}

inline float VBOVertex::length() const
{
    VBOVertex temp(*this);
    temp *= temp;
    return sqrt(temp.x + temp.y + temp.z);
}

inline std::ostream& operator<<(std::ostream& out, const VBOVertex& p)
{
    out << "(" << p.x << "," << p.y << "," << p.z << ")";
    return out;
}

inline std::istream& operator>>(std::istream& in, VBOVertex& p)
{
    in >> p.x;
    in >> p.y;
    in >> p.z;
    return in;
}

#endif // VBOVERTEX_H
