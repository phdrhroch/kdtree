#include "cudaglobaldefs.h"

#include "vbovertex.h"
#include "boundingbox.h"

__device__ __host__
inline OCTCODE zOrder(const VBOVertex& vertex, VBOVertex min, VBOVertex max)
{
    OCTCODE allNode = 0;

    VBOVertex point(vertex.x + vertex.x, vertex.y + vertex.y, vertex.z + vertex.z);

    for (int i = MAX_LEVEL; i >= 0; --i)
    {
        allNode <<= 3;
        VBOVertex center2(min.x + max.x, min.y + max.y, min.z + max.z);

        if(point.x > center2.x)
        {
            allNode |= 1;
            min.x = center2.x * 0.5f;
        }
        else
        {
            max.x = center2.x * 0.5f;
        }

        if(point.y > center2.y)
        {
            allNode |= 2;
            min.y = center2.y * 0.5f;
        }
        else
        {
            max.y = center2.y * 0.5f;
        }

        if(point.z > center2.z)
        {
            allNode |= 4;
            min.z = center2.z * 0.5f;
        }
        else
        {
            max.z = center2.z * 0.5f;
        }
    }

    return allNode;
}

void C_KDTreeFindMinMax(const KernelRunData& data, const VBOVertex* tree, const int treeSize, BoundingBox& minmax);

void C_KDTreeComputeOctcodes(const KernelRunData& data, const VBOVertex* tree, const int treeSize, const BoundingBox& bb, OCTCODE* octcodes, int* index);

void C_KDTreeOctcodesSort(const KernelRunData& data, OCTCODE* octcodes, int* index, int size);
