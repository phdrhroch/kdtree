#include "simpleview.h"

#include "camera.h"

#include <QMouseEvent>

SimpleView::SimpleView(const VBOVertex* vertices, int noVertices, float radius, int k):
    mVertices(vertices), mNoVertices(noVertices), mRadius(radius), mK(k)
{
    /*
    mCamera = new Camera(QVector3D(-5, 0, 0),
                         QVector3D(0, 0, 0),
                         QVector3D(0, 0, 1),
                         90.0f,
                         width(),
                         height());
                         */
    mCamera = new Camera(QVector3D(-5, 0, 0),
                         QVector3D(0, 0, 0),
                         QVector3D(0, 0, 1),
                         90.0f,
                         width(),
                         height());
}

void SimpleView::initializeGL()
{
    //mKdTree.allocateBuffers();
    mKdTree.buildKDTreeOnCPU(mVertices, mNoVertices);
    mKdTree.computeIndexOctcodesOnCPU();
    //mKdTree.computeIndexOctcodesOnGPU();
    mKdTree.computeNormalsOnGpu(mRadius, mK);

    pclTestNormals();
}

void SimpleView::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mCamera->apply();

    mKdTree.paintGL();
}

void SimpleView::resizeGL(int w, int h)
{
    mCamera->resize(w, h);
}

void SimpleView::mousePressEvent(QMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        mCamera->arcballpush(event->x(), event->y());
    }
    else if (event->button() == Qt::RightButton)
    {
        mCamera->movespush(event->x(), event->y());
    }
}

void SimpleView::mouseReleaseEvent(QMouseEvent* event)
{
    if(event->button() == Qt::LeftButton)
    {
        mCamera->arcballrelease(event->x(), event->y());
        updateGL();
    }
    else if (event->button() == Qt::RightButton)
    {
        mCamera->moverelease(event->x(), event->y());
        updateGL();
    }
}

void SimpleView::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        mCamera->arcballrelease(event->x(), event->y());
        updateGL();
    }
    else if (event->buttons() & Qt::RightButton)
    {
        mCamera->moverelease(event->x(), event->y());
        updateGL();
    }
}

void SimpleView::wheelEvent(QWheelEvent *event)
{
    mCamera->zoom(event->delta());
    updateGL();
}
