#ifndef SIMPLEVIEW_H
#define SIMPLEVIEW_H

#include "kdtreeinplace.h"
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

class Camera;

class SimpleView : public QGLWidget
{
public:
    SimpleView(const VBOVertex* vertices, int noVertices, float radius, int k);

    void setCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud);

protected:
    virtual void initializeGL();
    virtual void paintGL();
    virtual void resizeGL(int w, int h);

    // ovladani
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);
    void wheelEvent(QWheelEvent *event);

    void pclTestNormals();

private:
    KDTreeInplace mKdTree;

    const VBOVertex* mVertices;
    int mNoVertices;
    float mRadius;
    int mK;

    Camera* mCamera;

    pcl::PointCloud<pcl::PointXYZ>::Ptr mCloud;
    pcl::PointCloud<pcl::Normal>::Ptr mNormals;
};

#endif // SIMPLEVIEW_H
