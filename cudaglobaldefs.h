#ifndef CUDAGLOBALDEFS_H
#define CUDAGLOBALDEFS_H

class KernelRunData
{
public:
    KernelRunData(const dim3& grid, const dim3& block) :
        dimGrid(grid),
        dimBlock(block)

    {}

    dim3 dimGrid;
    dim3 dimBlock;
};

#endif // CUDAGLOBALDEFS_H
