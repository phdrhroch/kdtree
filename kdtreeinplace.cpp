#include "stdafx.h"

#include "kdtreeinplace.h"

#include "globaldefs.h"
#include "cudascopedtimer.h"

#include "kdtreeknn.cuh"
#include "kdtreecomputenormal.cuh"
#include "kdtreeoctcodescompute.cuh"


#include <QDebug>


template<typename T, typename S>
void qSort(T *v1, S *v2, int left, int right)
{
    // prevest na CUDA sort
    int i = left, j = right;
    T tmp;
    S tmpS;
    T pivot = v1[(left + right) / 2];

    while (i <= j)
    {
        while (v1[i] < pivot)
                i++;
        while (v1[j] > pivot)
                j--;
        if (i <= j)
        {
                tmp = v1[i];
                v1[i] = v1[j];
                v1[j] = tmp;

                tmpS = v2[i];
                v2[i] = v2[j];
                v2[j] = tmpS;

                i++;
                j--;
        }
    }

    if (left < j)
        qSort(v1, v2, left, j);
    if (i < right)
        qSort(v1, v2, i, right);
}

void findMinMax(const VBOVertex* vertices, VBOVertex& min, VBOVertex& max, int noVertices)
{
    min = vertices[0];
    max = vertices[0];
    for (int i = 1; i < noVertices; ++i)
    {
        if (vertices[i].x < min.x) min.x = vertices[i].x;
        if (vertices[i].y < min.y) min.y = vertices[i].y;
        if (vertices[i].z < min.z) min.z = vertices[i].z;

        if (vertices[i].x > max.x) max.x = vertices[i].x;
        if (vertices[i].y > max.y) max.y = vertices[i].y;
        if (vertices[i].z > max.z) max.z = vertices[i].z;
    }
}

void computeOctcodes(const VBOVertex* vertices, Indices& gpuindices, int noVertices)
{
    VBOVertex min;
    VBOVertex max;

    Octcodes octcodes;

    findMinMax(vertices, min, max, noVertices);

    octcodes.resize(noVertices);
    gpuindices.resize(noVertices);

    for (int i = 0; i < noVertices; ++i)
    {
        gpuindices[i] = i;
        octcodes[i] = zOrder(vertices[i], min, max);
    }

    qSort<OCTCODE, int>(&octcodes[0], &gpuindices[0], 0, noVertices-1);
}

void KDTreeInplace::bubbleSort(VBOVertex* vertices, int left, int right, unsigned char axis)
{
    VBOVertex tmp;
    for (int i = left; i < right; ++i)
    {
        for (int j = left; j < right; ++j)
        {
            if (vertices[j][axis] > vertices[j+1][axis])
            {
                tmp = vertices[j];
                vertices[j] = vertices[j+1];
                vertices[j+1] = tmp;
            }
        }
    }
}

void KDTreeInplace::quickSort(VBOVertex* vertices, int left, int right, unsigned char axis)
{
    int i = left, j = right;

    VBOVertex tmp;
    float pivot = vertices[(left + right) / 2][axis];

    do
    {
        while (vertices[i][axis] < pivot && i < right)
                i++;
        while (vertices[j][axis] > pivot && j > left)
                j--;
        if (i <= j)
        {
            tmp = vertices[i];
            vertices[i] = vertices[j];
            vertices[j] = tmp;

            i++;
            j--;
        }
    } while (i < j);

    if (left < j)
        quickSort(vertices, left, j, axis);
    if (i < right)
        quickSort(vertices, i, right, axis);

}

void KDTreeInplace::buildKDTreeOnCPU(const VBOVertex* vertices, unsigned int noVertices)
{
    mNoVertices = noVertices;

    if (mBuffersAllocated) freeBuffers();
    allocateBuffers();

    // indices init
    Vertices temp_vertices;
    temp_vertices.resize(noVertices);
    for (unsigned int i = 0; i < noVertices; ++i)
    {
        temp_vertices[i] = vertices[i];
    }

    // init tree
    Vertices tree;
    tree.resize(noVertices);
    // init vertice

    typedef std::queue<KDBuildData> BuildQueue;

    BuildQueue queue;
    queue.push(KDBuildData(0, 0, 0, noVertices-1));

    // can be changed into heapsort
    while(!queue.empty())
    {
        KDBuildData current = queue.front();
        queue.pop();

        if (current.from == current.to)
        {
            tree[current.nodeid] = temp_vertices[current.from];
            continue;
        }

        quickSort(&temp_vertices[0], current.from, current.to, current.axis);
        //bubbleSort(&temp_vertices[0], current.from, current.to, current.axis);

        int lbmedianpos = current.from + leftBalancedMedian(current.to - current.from + 1);

        tree[current.nodeid] = temp_vertices[lbmedianpos];

        unsigned char next_axis = (current.axis + 1) % 3;
        if (current.from <= lbmedianpos - 1)
            queue.push(KDBuildData(2*current.nodeid+1, next_axis, current.from, lbmedianpos - 1));

        if (lbmedianpos + 1 <= current.to)
            queue.push(KDBuildData(2*current.nodeid+2, next_axis, lbmedianpos + 1, current.to));
    }

    // copy tree into memory
    if (!mCudaTree.bind()) qDebug() << "mCudaTree.bind()";
    VBOVertex* gl_tree = (VBOVertex*)mCudaTree.map(QGLBuffer::WriteOnly);
    memcpy(gl_tree, &tree[0], sizeof(VBOVertex) * tree.size());
    mCudaTree.unmap();

    mTreeConstructed = true;
}

void KDTreeInplace::computeIndexOctcodesOnCPU()
{
    if (!mCudaTree.bind()) qDebug() << "mCudaTree.bind()";
    VBOVertex* gl_tree = (VBOVertex*)mCudaTree.map(QGLBuffer::ReadOnly);

    Indices indices;
    computeOctcodes(gl_tree, indices, mNoVertices);

    cudaMemcpy(cGpuIndices, &indices[0], sizeof(int) * mNoVertices, cudaMemcpyHostToDevice);
    gpuErrchk();

    mCudaTree.unmap();

    mOctcodesComputed = true;
}

void KDTreeInplace::computeIndexOctcodesOnGPU()
{
    if (!mTreeConstructed)
    {
        qDebug() << "KD-Tree must be constructed before octcodes computation";
    }

    unsigned int blocksize = 64;
    //unsigned int blocksize = 1;
    dim3 dimBlock(blocksize);
    dim3 dimGrid((mNoVertices + blocksize - 1) / blocksize);

    if (!mCudaTree.bind()) qDebug() << "mCudaTree.bind()";;
    VBOVertex* c_tree = (VBOVertex*)mCudaTree.mapCuda();

    {
        //CudaScopedTimer timer("KDTreeFindMinMax");
        C_KDTreeFindMinMax(KernelRunData(dimGrid, dimBlock), c_tree, mNoVertices, mMinMax);
        gpuErrchk();


        std::cout << mMinMax.min << std::endl;
        std::cout << mMinMax.max << std::endl;
    }

    OCTCODE* c_octcodes;
    cudaMalloc(&c_octcodes, sizeof(OCTCODE) * mNoVertices);

    {
        CudaScopedTimer timer("KDTreeComputeOctcodes");
        C_KDTreeComputeOctcodes(KernelRunData(dimGrid, dimBlock), c_tree, mNoVertices, mMinMax, c_octcodes, cGpuIndices);

    }

    {
        CudaScopedTimer timer("KDTreeOctcodesSort");
        C_KDTreeOctcodesSort(KernelRunData(dimGrid, dimBlock), c_octcodes, cGpuIndices, mNoVertices);

    }

    Octcodes test; test.resize(mNoVertices);
    cudaMemcpy(&test[0], c_octcodes, sizeof(OCTCODE) * mNoVertices, cudaMemcpyDeviceToHost);

    cudaFree(c_octcodes);

    OCTCODE test2 = test[0];
    for (int i = 1; i < test.size(); ++i)
    {
        if (test[i] < test2)
        {
            std::cout << "error" << std::endl;
        }
        else
        {
            std::cout << "ok" << std::endl;
        }

        test2 = test[i];
    }

    //cudaMemcpy(&minmax, cMinMax, sizeof(BoundingBox), cudaMemcpyDeviceToHost);
    //gpuErrchk();


    mCudaTree.unmapCuda();

    mOctcodesComputed = true;
}

void KDTreeInplace::allocateBuffers()
{
    if (!mCudaTree.create()) qDebug() << "mCudaTree.create()";
    if (!mCudaTree.bind()) qDebug() << "mCudaTree.bind()";
    mCudaTree.allocate(mNoVertices * sizeof(VBOVertex));
    mCudaTree.registerCuda();

    if (!mCudaNormals.create()) qDebug() << "mCudaNormals.create()";
    if (!mCudaNormals.bind()) qDebug() << "mCudaNormals.bind()";
    mCudaNormals.allocate(mNoVertices * sizeof(VBOVertex));
    mCudaNormals.registerCuda();

    cudaMalloc(&cGpuIndices, sizeof(int) * mNoVertices);
    gpuErrchk();

    mBuffersAllocated = true;
}

void KDTreeInplace::freeBuffers()
{
    mCudaTree.unregisterCuda();
    mCudaTree.destroy();

    mCudaNormals.unregisterCuda();
    mCudaNormals.destroy();

    cudaFree(cGpuIndices);
    gpuErrchk();

    mBuffersAllocated = false;
    mNormalsComputed = false;
    mTreeConstructed = false;
    mOctcodesComputed = false;
}

void KDTreeInplace::computeNormalsOnGpu(float range, unsigned int k)
{
    if (!mTreeConstructed)
    {
        qDebug() << "KD-Tree must be constructed before normal computation";
        return;
    }
    if (!mOctcodesComputed) computeIndexOctcodesOnGPU();

    mCudaTree.bind();
    VBOVertex* c_tree = (VBOVertex*)mCudaTree.mapCuda();

    mCudaNormals.bind();
    VBOVertex* c_normals = (VBOVertex*)mCudaNormals.mapCuda();


    // kernel call
    unsigned int blocksize = 64;
    //unsigned int blocksize = 1;
    dim3 dimBlock(blocksize);
    dim3 dimGrid((mNoVertices + blocksize - 1) / blocksize);

    int* c_neightbours;
    cudaMalloc(&c_neightbours, dimGrid.x * dimBlock.x * sizeof(int) * k);
    gpuErrchk();

    {
        CudaScopedTimer timer("KDTreeFindNN");
        C_KDTreeFindNN(KernelRunData(dimGrid, dimBlock), range, c_neightbours, c_tree, mNoVertices, k, cGpuIndices);
        gpuErrchk();
    }

    {
        CudaScopedTimer timer("KDTreeComputeNormal");
        C_KDTreeComputeNormal(KernelRunData(dimGrid, dimBlock), c_normals, c_neightbours, c_tree, mNoVertices, k, cGpuIndices);
        gpuErrchk();
    }


    mCudaTree.bind();
    mCudaTree.unmapCuda();

    mCudaNormals.bind();
    mCudaNormals.unmapCuda();

    mNormalsComputed = true;
}

void KDTreeInplace::paintGL()
{
    glEnable(GL_DEPTH_TEST);

    glPointSize(2.0f);
    glColor3f(1.0, 1.0, 1.0);
    mCudaTree.bind();
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(VBOVertex), 0);
    glDrawArrays(GL_POINTS, 0, mNoVertices);
    glDisableClientState(GL_VERTEX_ARRAY);


    mCudaTree.bind();
    VBOVertex* gl_tree = (VBOVertex*)mCudaTree.map(QGLBuffer::ReadOnly);

    mCudaNormals.bind();
    VBOVertex* gl_normals = (VBOVertex*)mCudaNormals.map(QGLBuffer::ReadOnly);

    glColor3f(1.0, 0.0, 0.0);
    for (int i = 0; i < mNoVertices; ++i)
    {
        glColor3f(fabs(gl_normals[i].x), fabs(gl_normals[i].y), fabs(gl_normals[i].z));
        glBegin(GL_LINES);
        glVertex3f(gl_tree[i].x, gl_tree[i].y, gl_tree[i].z);
        VBOVertex plusnormal = gl_tree[i] + gl_normals[i] * 0.1f;
        glVertex3f(plusnormal.x, plusnormal.y, plusnormal.z);
        glEnd();
    }


    /*
    srand(0);
    //glColor3f(0.0, 0.0, 1.0);
    for (int i = 0; i < mNoVertices; ++i)
    //for (int i = 0; i < 1; ++i)
    {
        glColor3f(float(rand()) / RAND_MAX, float(rand()) / RAND_MAX, float(rand()) / RAND_MAX);
        int k = 4;
        int blockDimx = 32;
        int blockIdxx = i / blockDimx;
        int threadIdxx = i % blockDimx;

        int index3 =  k * blockDimx * blockIdxx + threadIdxx + 3 * blockDimx;
        int index2 =  k * blockDimx * blockIdxx + threadIdxx + 2 * blockDimx;
        int index1 =  k * blockDimx * blockIdxx + threadIdxx + 1 * blockDimx;
        int index0 =  k * blockDimx * blockIdxx + threadIdxx + 0 * blockDimx;

        glBegin(GL_LINES);
        glVertex3fv((const float*)&gl_tree[testindices[index3]]);
        glVertex3fv((const float*)&gl_tree[testindices[index1]]);
        glVertex3fv((const float*)&gl_tree[testindices[index3]]);
        glVertex3fv((const float*)&gl_tree[testindices[index2]]);
        glVertex3fv((const float*)&gl_tree[testindices[index0]]);
        glVertex3fv((const float*)&gl_tree[testindices[index3]]);
        glEnd();
    }
    */

    mCudaTree.bind(); mCudaTree.unmap();
    mCudaNormals.bind(); mCudaNormals.unmap();
}
