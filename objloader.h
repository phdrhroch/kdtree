#ifndef OBJLOADER_H
#define OBJLOADER_H

#include "vbovertex.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>

class ObjLoader
{
public:
    ObjLoader();

    bool load(const char* filename);
    bool loadFromPCL(const pcl::PointCloud<pcl::PointXYZ>& pcl);

    const VBOVertex* vertices() const { return &mVertices[0]; }
    int size() const { return mVertices.size(); }
    bool loaded() const { return mLoaded; }

private:
    Vertices mVertices;
    bool mLoaded;

};

#endif // OBJLOADER_H
