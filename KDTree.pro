#-------------------------------------------------
#
# Project created by QtCreator 2013-04-02T20:01:46
#
#-------------------------------------------------
TARGET = KDTree
CONFIG += console

DESTDIR = release
OBJECTS_DIR = release/obj
CUDA_OBJECTS_DIR = release/cuda

QT += core gui opengl
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

#QMAKE_CXXFLAGS += -fopenmp

HEADERS  += stdafx.h \
    vbovertex.h \
    globaldefs.h \
    kdtreeinplace.h \
    kdtreeknn.cuh \
    kdtreecomputenormal.cuh \
    cudaglobaldefs.h \
    qglbuffercuda.h \
    simpleview.h \
    camera.h \
    trackball.h \
    cudascopedtimer.h \
    kdtreeoctcodescompute.cuh \
    boundingbox.h \
    kdtreeflipnormal.cuh

SOURCES += main.cpp \
    kdtreeinplace.cpp \
    objloader.cpp \
    qglbuffercuda.cpp \
    simpleview.cpp \
    camera.cpp \
    trackball.cpp \
    cudascopedtimer.cpp \
    simpleviewpcl.cpp

OTHER_FILES += \
    kdtreeoctcodescompute.cu \
    kdtreeknn.cu \
    kdtreecomputenormal.cu \
    kdtreeflipnormal.cu

# CUDA settings <-- may change depending on your system
CUDA_SOURCES += kdtreeknn.cu kdtreecomputenormal.cu kdtreeoctcodescompute.cu kdtreeflipnormal.cu
#CUDA_ARCH = compute_20           # Type of CUDA architecture, for example 'compute_10', 'compute_11', 'sm_10'
#NVCC_OPTIONS = -ccbin \"C:/Program Files (x86)/Microsoft Visual Studio 9.0/VC/bin\" --use_fast_math -deviceemu
#NVCC_OPTIONS = --use_fast_math

INCLUDEPATH += "$$(PCL_INC_PATH)"
INCLUDEPATH += "$$(EIGEN_INC_PATH)"

win32:CONFIG(release, debug|release): LIBS += -L"$$(PCL_LIB_PATH)" -lpcl_common_release -lpcl_surface_release -lpcl_surface_release -lpcl_kdtree_release -lpcl_search_release -lpcl_features_release -lpcl_io_release
win32:CONFIG(debug, debug|release): LIBS += -L"$$(PCL_LIB_PATH)" -lpcl_common_debug -lpcl_surface_debug -lpcl_surface_debug -lpcl_kdtree_debug -lpcl_search_debug -lpcl_features_debug -lpcl_io_debug
linux-g++-64: LIBS += -L"$$(PCL_LIB_PATH)" -lpcl_common -lpcl_surface -lpcl_surface -lpcl_kdtree -lpcl_search -lpcl_features -lpcl_io -lboost_system


# include paths
INCLUDEPATH += "$$(CUDA_INC_PATH)"

# library directories
QMAKE_LIBDIR += "$$(CUDA_LIB_PATH)"

LIBS += -lcuda -lcudart

CUDA_INC = $$join(INCLUDEPATH,'" -I"','-I"','"')
#CUDA_INC = ""

# Configuration of the Cuda compiler
CONFIG(debug, debug|release) {
    # Debug mode
    cuda_d.input = CUDA_SOURCES
    cuda_d.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda_d.commands = nvcc -D_DEBUG -g -G --machine $$(SYSTEM_TYPE) $$CUDA_INC -arch=$$(CUDA_ARCH) -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} $$(NVCC_OPTIONS)
    cuda_d.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda_d
}
else {
    # Release mode
    cuda.input = CUDA_SOURCES
    cuda.output = $$CUDA_OBJECTS_DIR/${QMAKE_FILE_BASE}_cuda.o
    cuda.commands = nvcc --machine $$(SYSTEM_TYPE) $$CUDA_INC -arch=$$(CUDA_ARCH) -c -o ${QMAKE_FILE_OUT} ${QMAKE_FILE_NAME} $$(NVCC_OPTIONS) $$(QMAKE_INCDIR_QT)
    cuda.dependency_type = TYPE_C
    QMAKE_EXTRA_COMPILERS += cuda
}
