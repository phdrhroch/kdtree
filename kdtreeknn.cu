#include "kdtreeknn.cuh"

#include "globaldefs.h"

#define NODEMASK    0x1FFFFFFF
#define AXISMASK    0x60000000
#define ONSIDEMASK  0x80000000

#define XAXIS 0x00000000
#define YAXIS 0x20000000
#define ZAXIS 0x40000000
#define MODULOAXIS 0x60000000
#define ROTATIONAXIS 29

#define ONSIDE 0x80000000
#define OFFSIDE 0x00000000

texture<float4, 1, cudaReadModeElementType> texture_tree;

__constant__ int prevaxistable_index[3] = {2, 0, 1};

int C_KDTreeFindNN_stacksize(int n)
{
    // h = floor(log2(n))
    unsigned int tempn = n;
    unsigned int h = 0;
    while (tempn>>=1)h++;

    // best result is buffered
    return h;
}

template <unsigned int axis>
__device__
inline void onsideoffside(const VBOVertex* point, const VBOVertex* vertex, int node, int& onsidenode, int& offsidenode)
{
    unsigned int naxis;
    switch (axis)
    {
    case XAXIS:
        if (point->x < vertex->x)
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
        }
        else if (point->x > vertex->x)
        {
            onsidenode |= (node * 2) + 2;
            offsidenode |= (node * 2) + 1;
        }
        else
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
            offsidenode |= ONSIDE;
        }
        naxis = YAXIS;
        break;
    case YAXIS:
        if (point->y < vertex->y)
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
        }
        else if (point->y > vertex->y)
        {
            onsidenode |= (node * 2) + 2;
            offsidenode |= (node * 2) + 1;
        }
        else
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
            offsidenode |= ONSIDE;
        }
        naxis = ZAXIS;
        break;
    case ZAXIS:
        if (point->z < vertex->z)
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
        }
        else if (point->z > vertex->z)
        {
            onsidenode |= (node * 2) + 2;
            offsidenode |= (node * 2) + 1;
        }
        else
        {
            onsidenode |= (node * 2) + 1;
            offsidenode |= (node * 2) + 2;
            offsidenode |= ONSIDE;
        }
        naxis = XAXIS;
        break;
    }

    onsidenode |= naxis;
    offsidenode |= naxis;
}

__device__
inline float axisdistance2(const VBOVertex* v1, const VBOVertex* v2, unsigned int axis)
{
    axis>>=ROTATIONAXIS;
    float result = (*v1)[axis] - (*v2)[axis];
    return result * result;
}

__device__
inline float axisdistance2prev(const VBOVertex* v1, const VBOVertex* v2, unsigned int axis)
{
    axis>>=ROTATIONAXIS;
    axis = prevaxistable_index[axis];

    float result = (*v1)[axis] - (*v2)[axis];
    return result * result;

}

__device__
inline float distance2(const VBOVertex* v1, const VBOVertex* v2)
{
    return (v1->x - v2->x)*(v1->x - v2->x) + (v1->y - v2->y)*(v1->y - v2->y) + (v1->z - v2->z)*(v1->z - v2->z);
}

__device__
inline void bestresult_insert(bestresult* bestresults, bestresult currentresult, int& numfound, int k)
{
    if (numfound < k)
    {
        for (int j = 1; j < numfound; j++)
        {
            if (currentresult.dist > bestresults[j * blockDim.x].dist)
            {
                bestresult tmp = currentresult;
                currentresult = bestresults[j * blockDim.x];
                bestresults[j * blockDim.x] = tmp;
            }
        }

        bestresults[numfound * blockDim.x] = currentresult;
        numfound++;
    }
    else
    {
        for (int j = k-1; j >= 0; j--)
        {
            if (currentresult.dist < bestresults[j * blockDim.x].dist)
            {
                bestresult tmp = currentresult;
                currentresult = bestresults[j * blockDim.x];
                bestresults[j * blockDim.x] = tmp;
            }
        }
    }
}

__device__
inline void stackpush(int* stack, int& stacktop, int nodeid)
{
    stack[stacktop] = nodeid;
    stacktop += blockDim.x;
}

__device__
inline bool stackempty(int stacktop)
{
    return stacktop == threadIdx.x;
}

__device__
inline int stacktoppop(int* stack, int& stacktop)
{
    stacktop -= blockDim.x;
    return stack[stacktop];
}

__global__
void Cuda_KDTreeFindNN(float range, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index)
{
    // get index of thread in grid
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // check if thread is valid
    if (idx >= treeSize) return;

    // get point from global memory
    //const VBOVertex point = tree[idx];
    //const VBOVertex point = tree[index[idx]];
    const VBOVertex point = tex1Dfetch(texture_tree, index[idx]);

    extern __shared__ bestresult shared_memory[];

    // configure best results output
    bestresult* bestresults = shared_memory + threadIdx.x;

    // configure shared memory stack
    int stacktop = threadIdx.x;
    int* stack = (int*)&shared_memory[k * blockDim.x];

    // stacktop cache
    int stacktopcache = 0 | XAXIS | ONSIDE;

    // zero best result is max dist
    bestresults[0].index = -1;
    bestresults[0].dist = range * range;
    int numfound = 1;

    for(;;)
    {
        int currentnode = stacktopcache & NODEMASK;
        int currentaxis = stacktopcache & AXISMASK;

        //const VBOVertex currentvertex = tree[currentnode];
        const VBOVertex currentvertex = tex1Dfetch(texture_tree, currentnode);

        // trim offside
        if (((stacktopcache & ONSIDEMASK) == OFFSIDE) && (axisdistance2prev(&tree[currentnode>>1], &point, currentaxis) > bestresults[0].dist))
        {  // load next from stack

            // test if stack is empty
            if (stackempty(stacktop)) break;

            stacktopcache = stacktoppop(stack, stacktop);
            continue;
        }


        // test and insert result
        bestresult currentresult = {currentnode, distance2(&currentvertex, &point)};
        if (currentresult.dist < bestresults[0].dist)
        {
            bestresult_insert(bestresults, currentresult, numfound, k);
        }

        // define onside and offside node
        int onsidenode = ONSIDE;
        int offsidenode = OFFSIDE;

        switch (currentaxis)
        {
        case XAXIS:
            onsideoffside<XAXIS>(&point, &currentvertex, currentnode, onsidenode, offsidenode);
            break;
        case YAXIS:
            onsideoffside<YAXIS>(&point, &currentvertex, currentnode, onsidenode, offsidenode);
            break;
        case ZAXIS:
            onsideoffside<ZAXIS>(&point, &currentvertex, currentnode, onsidenode, offsidenode);
        }

        bool checkoffside = false;
        // check offside node and push
        if ((offsidenode & NODEMASK) < treeSize)
        {
            // trim
            if (((stacktopcache & ONSIDEMASK) != OFFSIDE) || !(axisdistance2(&tree[offsidenode & NODEMASK], &point, currentaxis) > bestresults[0].dist))
            {
                stacktopcache = offsidenode;
                checkoffside = true;
            }
        }

        bool checkonside = (onsidenode & NODEMASK) < treeSize;
        // push children onto stack and possibly trim offside node
        if (checkonside)
        {
            stacktopcache = onsidenode;
            if (checkoffside)
            { // check if we doesnt have to push offside from cache
                stackpush(stack, stacktop, offsidenode);
            }
        }

        if (!checkonside && !checkoffside)
        {
            if (stackempty(stacktop)) break;

            stacktopcache = stacktoppop(stack, stacktop);
        }
    }

    for (int i = 0; i < k; ++i)
    {
        neightbours[k * blockDim.x * blockIdx.x + threadIdx.x + i * blockDim.x] = bestresults[i * blockDim.x].index;
        //indices[idx * k + i] = bestresults[i * blockDim.x].index;
    }
}

void C_KDTreeFindNN(const KernelRunData& kernel, float range, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index)
{
    unsigned int dimStack = (C_KDTreeFindNN_stacksize(treeSize) * kernel.dimBlock.x * sizeof(unsigned int)) + (kernel.dimBlock.x * k * sizeof(bestresult));

    //printf("C_KDTreeFindNN: stacksize = %d\n", dimStack);

    cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<float4>();

    cudaBindTexture(0, texture_tree, tree, channelDesc, 4*4*treeSize);
    gpuErrchk();

    Cuda_KDTreeFindNN<<<kernel.dimGrid, kernel.dimBlock, dimStack>>>(range, neightbours, tree, treeSize, k, index);
    gpuErrchk();

    cudaUnbindTexture(texture_tree);
    gpuErrchk();
}


