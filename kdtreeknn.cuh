#include "cudaglobaldefs.h"

#include "vbovertex.h"

struct bestresult
{
    int index;
    float dist;
};

void C_KDTreeFindNN(const KernelRunData& kernel, float range, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index);
