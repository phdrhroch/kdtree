#include "cudascopedtimer.h"

#include <stdio.h>
#include <stdlib.h>

CudaScopedTimer::CudaScopedTimer(const char* kernelName) :
mKernelName(kernelName)
{
    cudaEventCreate(&mStart);
    cudaEventCreate(&mStop);

    cudaEventRecord(mStart, 0);
}

CudaScopedTimer::~CudaScopedTimer()
{
    cudaEventRecord(mStop, 0);
    cudaEventSynchronize(mStop);

    float time;
    cudaEventElapsedTime(&time, mStart, mStop);
    printf("Time executing kernel - %s: %f ms\n", mKernelName, time);

    cudaEventDestroy(mStart);
    cudaEventDestroy(mStop);
}
