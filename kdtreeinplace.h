#ifndef KDTREEUINPLACE_H
#define KDTREEUINPLACE_H

#include "globaldefs.h"
#include "vbovertex.h"
#include "boundingbox.h"
#include <math.h>
#include <float.h>

#include <queue>
#include <stack>
#include <vector>

#include "qglbuffercuda.h"

typedef std::vector<VBOVertex> Vertices;
typedef std::vector<int> Indices;

class KDTreeInplace
{
public:
    KDTreeInplace() :
        mNoVertices(0),
        mTreeConstructed(false),
        mBuffersAllocated(false),
        mNormalsComputed(false),
        mOctcodesComputed(false)
    {}

    void buildKDTreeOnCPU(const VBOVertex* vertices,  unsigned int noVertices);
    void computeIndexOctcodesOnCPU();

    void computeIndexOctcodesOnGPU();

    //void kNN(float range, unsigned int k = 3);

    void computeNormalsOnGpu(float range, unsigned int k = 3);

    typedef std::vector<VBOVertex> Tree;

    void paintGL();

    QGLBufferCuda* cudaNormals() { return &mCudaNormals; }
private:
    void allocateBuffers();
    void freeBuffers();

    struct KDBuildData
    {
        KDBuildData(int nodeid_, char axis_, int from_, int to_) :
            nodeid(nodeid_),
            axis(axis_),
            from(from_),
            to(to_) {}
        int nodeid;
        unsigned char axis;
        int from;
        int to;
    };

    struct KDSearchData
    {
        KDSearchData(unsigned int nodeid_, unsigned char axis_, bool onside_):
            nodeid(nodeid_),
            axis(axis_),
            onside(onside_){}

        KDSearchData() {}

        unsigned int nodeid;
        unsigned char axis;
        bool onside;
    };

    static bool axisLess(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis);

    static bool axisMore(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis);

    static float axisDistance(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis);

    static float axisDistance2(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis);

    static void quickSort(VBOVertex* vertices, int left, int right, unsigned char axis);

    static void bubbleSort(VBOVertex* vertices, int left, int right, unsigned char axis);

    static int leftBalancedMedian(unsigned int n);

private:
    //Tree mTree;
    //Indices mGpuIndices;

    QGLBufferCuda mCudaTree;
    QGLBufferCuda mCudaNormals;

    //VBOVertex* cTree;
    //VBOVertex* cNormals;

    int* cGpuIndices;

    BoundingBox mMinMax;

    int mNoVertices;

    bool mBuffersAllocated;
    bool mTreeConstructed;
    bool mNormalsComputed;
    bool mOctcodesComputed;

};

inline bool KDTreeInplace::axisLess(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis)
{
    // prevest na pointer aritmetiku
    if (axis == 0)
    {
        return v1->x < v2->x;
    }
    else if (axis == 1)
    {
        return v1->y < v2->y;
    }
    else
    {
        return v1->z < v2->z;
    }
}

inline  bool KDTreeInplace::axisMore(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis)
{
    // prevest na pointer aritmetiku
    if (axis == 0)
    {
        return v1->x > v2->x;
    }
    else if (axis == 1)
    {
        return v1->y > v2->y;
    }
    else
    {
        return v1->z > v2->z;
    }
}

inline float KDTreeInplace::axisDistance(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis)
{
    if (axis == 0) return fabs(v1->x - v2->x);
    else if (axis == 1) return fabs(v1->y - v2->y);
    else return fabs(v1->z - v2->z);
}

inline float KDTreeInplace::axisDistance2(const VBOVertex* v1, const VBOVertex* v2, unsigned char axis)
{
    if (axis == 0) return (v1->x - v2->x)*(v1->x - v2->x);
    else if (axis == 1) return (v1->y - v2->y)*(v1->y - v2->y);
    else return (v1->z - v2->z)*(v1->z - v2->z);
}

inline int KDTreeInplace::leftBalancedMedian(unsigned int n)
{
    // h = floor(log2(n))
    unsigned int tempn = n;
    unsigned int h = 0;
    while (tempn>>=1)h++;

    // m = 2^h
    unsigned int m = 1<<h;

    // reminder
    unsigned r = n - (m - 1);

    if (r < m / 2) return (m-2)/2 + r;
    else return (m-2)/2 + m/2;
}


#endif // KDTREEUINPLACE_H
