#ifndef QGLBUFFERCUDA_H
#define QGLBUFFERCUDA_H

#include <QGLBuffer>

class QGLBufferCuda : public QGLBuffer
{
public:
    QGLBufferCuda();
    void registerCuda();
    void unregisterCuda();
    void* mapCuda();
    void unmapCuda();
};

#endif // QGLBUFFERCUDA_H
