#ifndef GLOBALDEFS_H
#define GLOBALDEFS_H

#include "vbovertex.h"

#include <iostream>
#include <stdlib.h>

#define gpuErrchk() { gpuAssert( (char *)__FILE__, __LINE__); }
inline void gpuAssert(char *file, int line, bool abort=true)
{
    cudaError_t code = cudaGetLastError();
   if (code != cudaSuccess)
   {
       std::cerr << "GPU assert: " << cudaGetErrorString(code) << " " << file << " " << line << std::endl;
      if (abort) exit(code);
   }
}

#define MAX_LEVEL 20

#endif // GLOBALDEFS_H
