#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H

#include "vbovertex.h"

struct BoundingBox
{
    VBOVertex min;
    VBOVertex max;

    void expand(const VBOVertex& v);

    // defined in kdtreeoctcodescompute.cu
    __device__ __host__ static BoundingBox minmax(const BoundingBox& b1, const BoundingBox& b2);
    __device__ __host__ static BoundingBox minmax(const VBOVertex& b1, const VBOVertex& b2);
};

inline void BoundingBox::expand(const VBOVertex& v)
{
    if (v.x < min.x) min.x = v.x;
    if (v.y < min.y) min.y = v.y;
    if (v.z < min.z) min.z = v.z;

    if (v.x > max.x) max.x = v.x;
    if (v.y > max.y) max.y = v.y;
    if (v.z > max.z) max.z = v.z;
}

#endif // BOUNDINGBOX_H
