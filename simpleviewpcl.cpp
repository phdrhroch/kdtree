#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>

#include "simpleview.h"

#include <QDebug>
#include <QTime>

void SimpleView::setCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud)
{
    mCloud = cloud;
    mNormals = pcl::PointCloud<pcl::Normal>::Ptr(new pcl::PointCloud<pcl::Normal>);

    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;

    /*
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud (cloud);
    n.setInputCloud (cloud);
    n.setSearchMethod (tree);
    n.setKSearch (mK);

    QTime timer; timer.start();
    n.compute (*mNormals);
    qDebug() << "Normals pcl computing on cpu" << timer.elapsed() << "ms";
    */
}

float distanceSquared(const pcl::Normal &p1, const VBOVertex &p2)
{
    float dx = p1.normal_x - p2.x;
    float dy = p1.normal_y - p2.y;
    float dz = p1.normal_z - p2.z;

    return dx*dx + dy*dy + dz*dz;
}

void SimpleView::pclTestNormals()
{
    return;
    QGLBufferCuda* gl_normals = mKdTree.cudaNormals();
    gl_normals->bind();
    VBOVertex* normals = (VBOVertex*)gl_normals->map(QGLBuffer::ReadOnly);

    int error = 0;
    for (int i = 0; i < mNoVertices; ++i)
    {
        VBOVertex normal1 = normals[i];
        pcl::Normal normal2 = (*mNormals)[i];

        //if (distanceSquared((*mNormals)[i], normals[i]) > 1e-6)
        if (distanceSquared(normal2, normal1) > 1e-6)
        {
            std::cerr<<i<<std::endl;
            std::cerr<<normal1<<";"<<normal2<<std::endl;
            error++;
        }
    }
    qDebug() << "Points:" << mNoVertices << "Errors:" << error;

    gl_normals->unmap();
}
