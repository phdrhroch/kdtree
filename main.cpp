#include "vbovertex.h"

#include <pcl/point_types.h>
#include <pcl/io/ply_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/surface/gp3.h>
#include <pcl/point_cloud.h>
/*
#include <pcl/surface/impl/gp3.hpp>
#include <pcl/features/impl/normal_3d.hpp>
#include <pcl/kdtree/impl/kdtree_flann.hpp>
#include <pcl/search/impl/organized.hpp>
*/

//-f /home/filip/Skola/DP/KDTree/bunny/data/bun000.ply
/*
POINT_CLOUD_REGISTER_POINT_STRUCT(VBOVertex,
                                  (float, x, x)
                                  (float, y, y)
                                  (float, z, z))
*/

int
main2 (int argc, char** argv)
{
  // Load input file into a PointCloud<T> with an appropriate type
  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);
  //pcl::PointCloud<VBOVertex>::Ptr cloud(new pcl::PointCloud<VBOVertex>);

  //pcl::PCLPointCloud2 cloud_blob;
  //pcl::io::loadPCDFile ("bun0.pcd", cloud_blob);
  //pcl::fromPCLPointCloud2 (cloud_blob, *cloud);
  //* the data should be available in cloud

  // Normal estimation*
  pcl::NormalEstimationOMP<pcl::PointXYZ, pcl::Normal> n;
  pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
  pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
  tree->setInputCloud (cloud);
  n.setInputCloud (cloud);
  n.setSearchMethod (tree);
  n.setKSearch (20);
  n.compute (*normals);
  //* normals should not contain the point normals + surface curvatures

  // Concatenate the XYZ and normal fields*
  pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
  pcl::concatenateFields (*cloud, *normals, *cloud_with_normals);
  //* cloud_with_normals = cloud + normals

  // Create search tree*
  pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
  tree2->setInputCloud (cloud_with_normals);

  // Initialize objects
  pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
  pcl::PolygonMesh triangles;

  // Set the maximum distance between connected points (maximum edge length)
  gp3.setSearchRadius (0.025);

  // Set typical values for the parameters
  gp3.setMu (2.5);
  gp3.setMaximumNearestNeighbors (100);
  gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
  gp3.setMinimumAngle(M_PI/18); // 10 degrees
  gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
  gp3.setNormalConsistency(false);

  // Get result
  gp3.setInputCloud (cloud_with_normals);
  gp3.setSearchMethod (tree2);
  gp3.reconstruct (triangles);

  // Additional vertex information
  std::vector<int> parts = gp3.getPartIDs();
  std::vector<int> states = gp3.getPointStates();

  // Finish
  return (0);
}

#include "stdafx.h"

#include "globaldefs.h"
#include "kdtreeinplace.h"
#include "objloader.h"

#include "simpleview.h"
#include <iostream>

#include <QApplication>

#include <QDebug>

#define qDebug() std::cout

void randomFill(Vertices& vertices, unsigned int size, pcl::PointCloud<pcl::PointXYZ>& cloud)
{
    vertices.resize(size);// = new VBOVertex[no_vertices];
    memset(&vertices[0], 0, sizeof(VBOVertex) * size);

    cloud.resize(size);

    float r;
    unsigned int i,j;
    float range_max = 0.5f;
    float range_min = 0.0f;
    VBOVertex *ptr = &vertices[0];
    pcl::PointXYZ* ptr2 = &cloud[0];

    int face;
    float rnd1, rnd2;
    for (i=0; i<size; i++, ptr++)
    {
        face = rand()%6;
        rnd1 = (float)((double)rand() / (RAND_MAX));
        rnd2 = (float)((double)rand() / (RAND_MAX));

        switch (face)
        {
        case 0:  //front face
            ptr->x = rnd1;
            ptr->y = 0.0f;
            ptr->z = rnd2;

            ptr2->x = rnd1;
            ptr2->y = 0.0f;
            ptr2->z = rnd2;

            break;
        case 1:  //back face
            ptr->x = rnd1;
            ptr->y = 1.0f;
            ptr->z = rnd2;

            ptr2->x = rnd1;
            ptr2->y = 1.0f;
            ptr2->z = rnd2;
            break;
        case 2:  //left face
            ptr->x = 0.0f;
            ptr->y = rnd1;
            ptr->z = rnd2;

            ptr2->x = 0.0f;
            ptr2->y = rnd1;
            ptr2->z = rnd2;
            break;
        case 3:  //right face
            ptr->x = 1.0f;
            ptr->y = rnd1;
            ptr->z = rnd2;

            ptr2->x = 1.0f;
            ptr2->y = rnd1;
            ptr2->z = rnd2;
            break;
        case 4:  //bottom face
            ptr->x = rnd1;
            ptr->y = rnd2;
            ptr->z = 0.0f;

            ptr2->x = rnd1;
            ptr2->y = rnd2;
            ptr2->z = 0.0f;
            break;
        case 5:  //right face
            ptr->x = rnd1;
            ptr->y = rnd2;
            ptr->z = 1.0f;

            ptr2->x = rnd1;
            ptr2->y = rnd2;
            ptr2->z = 1.0f;
            break;
        }
    }
}

void randomFill(std::vector<float>& ranges, unsigned int size)
{
    ranges.resize(size);// = new VBOVertex[no_vertices];
    memset(&ranges[0], 0, sizeof(float) * size);

    float *ptr = &ranges[0];
    for (unsigned int i=0; i<size; i++, ptr++)
    {
        float rnd = (float)((double)rand() / (RAND_MAX));
        *ptr = rnd;
    }
}

bool testSearchCudaInplace(KDTreeInplace* kdtree)
{
    bool result = true;

    float range = 1000.0f;

    //QTime timer;
    //timer.start();
    kdtree->computeNormalsOnGpu(range, 4);

    //qDebug() << "KD CUDA time:" << timer.elapsed() << "ms";

    //timer.start();
    //kdtree->kNN(range, 4);
    //qDebug() << "KD OMP time:" << timer.elapsed() << "ms";

    return result;
}
int main(int argc, char *argv[])
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud (new pcl::PointCloud<pcl::PointXYZ>);

    QApplication app(argc, argv);
    QStringList arguments = app.arguments();

    ObjLoader loader;
    int no_vertices = 1000000;
    float radius = FLT_MAX;
    int k = 4;

    bool ok;

    if (arguments.size() > 1)
    {   
        if (arguments.size() > 2)
        {
            if (arguments[1] == "-f")
            {
                qDebug() << "loading ply file";
                try
                {
                    pcl::io::loadPLYFile(argv[2],*cloud);
                }
                catch(std::exception& ex)
                {
                    std::cout << ex.what() << std::endl;
                    return 0;
                }
                qDebug() << "loaded";

                //loader.load(argv[2]);
                loader.loadFromPCL(*cloud);
                if(!loader.loaded())
                {
                    qDebug() << "cannot load file:" << argv[2];
                    return 1;
                }

                if (arguments.size() > 3)
                {
                    radius = arguments[3].toFloat(&ok);
                    if (!ok)
                    {
                        qDebug() << "wrong param radius:" << argv[3];
                        return 1;
                    }
                }

                if (arguments.size() > 4)
                {
                    k = arguments[4].toInt(&ok);
                    if (!ok)
                    {
                        qDebug() << "wrong param k:" << argv[3];
                        return 1;
                    }
                }
            }
            else
            {
                no_vertices = arguments[1].toInt(&ok);
                if (!ok)
                {
                    qDebug() << "wrong param number of vertices:" << argv[1];
                    return 1;
                }

                radius = arguments[2].toFloat(&ok);
                if (!ok)
                {
                    qDebug() << "wrong param radius:" << argv[3];
                    return 1;
                }

                if (arguments.size() > 3)
                {
                    k = arguments[3].toInt(&ok);
                    if (!ok)
                    {
                        qDebug() << "wrong param k:" << argv[3];
                        return 1;
                    }
                }
            }
        }
        else
        {
            no_vertices = arguments[1].toInt(&ok);
            if (!ok)
            {
                qDebug() << "wrong param number of vertices:" << argv[1];
                return 1;
            }
        }
    }

    if (loader.loaded())
    {
        qDebug() << "filename:" << argv[2] << "no vertices:" << loader.size() << "radius:" << radius << "k:" << k;

        SimpleView view (loader.vertices(), loader.size(), radius, k);
        view.setCloud(cloud);
        view.show();
        return app.exec();
    }
    else
    {
        //srand(time(NULL));
        qDebug() << "no vertices:" << no_vertices << "radius:" << radius << "k:" << k;

        srand(0);

        Vertices vertices;
        randomFill(vertices, no_vertices, *cloud);

        SimpleView view(&(vertices)[0], vertices.size(), radius, k);
        view.setCloud(cloud);
        view.show();
        return app.exec();
    }
}
