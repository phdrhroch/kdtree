#include "cudaglobaldefs.h"

#include "vbovertex.h"

struct Matrix3x3
{
private:
    float data[9];
    float padding[3];
public:
    __device__ Matrix3x3();
    __device__ Matrix3x3(const Matrix3x3& from);

    __device__ float operator()(int row, int column) const;
    __device__ float& operator()(int row, int column);

    __device__ float* operator[](int row);
    __device__ const float* operator[](int row) const;

    __device__ Matrix3x3& operator *=(float f);

    __device__ float determinant() const;
    __device__ VBOVertex columnsCrossProduct(int cola, int colb);
};

inline Matrix3x3::Matrix3x3(const Matrix3x3& from)
{
    memcpy(data, from.data, sizeof(float) * 9);
}

inline Matrix3x3::Matrix3x3()
{
    memset(data, 0, sizeof(float) * 9);
}

inline float Matrix3x3::operator()(int row, int column) const
{
    return data[row * 3 + column];
}

inline float& Matrix3x3::operator()(int row, int column)
{
    return data[row * 3 + column];
}

inline Matrix3x3& Matrix3x3::operator *=(float f)
{
    for (int i = 0; i < 9; ++i) data[i] *= f;
    return *this;
}

inline float* Matrix3x3::operator[](int row)
{
    return data + (row * 3);
}

inline const float* Matrix3x3::operator[](int row) const
{
    return data + (row * 3);
}

inline float Matrix3x3::determinant() const
{
    /*return (a[0][0] * (a[1][1] * a[2][2] - a[2][1] * a[1][2])
           -a[1][0] * (a[0][1] * a[2][2] - a[2][1] * a[0][2])
           +a[2][0] * (a[0][1] * a[1][2] - a[1][1] * a[0][2]))
           */
    return (data[0] * (data[4] * data[8] - data[7] * data[5])
            -data[3] * (data[1] * data[8] - data[7] * data[3])
            +data[6] * (data[1] * data[5] - data[4] * data[3]));
}

inline VBOVertex Matrix3x3::columnsCrossProduct(int cola, int colb)
{
    VBOVertex result;
    result.x = data[3 + cola]*data[6 + colb] - data[3 + colb]*data[6+cola];
    result.y =data[6  + cola]*data[colb] - data[6 + colb]*data[cola];
    result.z = data[cola]*data[3 + colb] - data[colb]*data[3+cola];
    return result;

}

void C_KDTreeComputeNormal(const KernelRunData& data, VBOVertex* normals, int* neightbours, const VBOVertex* tree, const int treeSize, const int k, const int* index);
