#include "objloader.h"

#include <fstream>
#include <string>
#include <sstream>
#include <iostream>

ObjLoader::ObjLoader()
{
    mLoaded = false;
}

bool ObjLoader::load(const char* filename)
{
    // load all points from OBJ file
    std::ifstream fs(filename);

    if (!fs.is_open()) return false;

    while(fs.good())
    {
        std::string line;
        std::getline(fs, line);

        if (line.size() == 0 || line[0] == '#') continue;

        std::stringstream ss(line);
        std::string token;

        ss >> token;

        if (token == "v")
        {
            VBOVertex vertex;
            ss >> vertex;
            mVertices.push_back(vertex);
        }
    }

    mLoaded = true;

    return true;
}

bool ObjLoader::loadFromPCL(const pcl::PointCloud<pcl::PointXYZ>& pcl)
{
    for (unsigned int i = 0; i < pcl.size(); ++i)
    {
        VBOVertex vertex (pcl[i].x, pcl[i].y, pcl[i].z);
        mVertices.push_back(vertex);
    }

    mLoaded = true;

    return true;
}
