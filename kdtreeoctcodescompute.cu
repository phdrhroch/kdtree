#include "kdtreeoctcodescompute.cuh"
#include "globaldefs.h"

__device__ __host__
inline BoundingBox BoundingBox::minmax(const BoundingBox& b1, const BoundingBox& b2)
{
    BoundingBox result;

    result.min.x = fminf(b1.min.x, b2.min.x);
    result.min.y = fminf(b1.min.y, b2.min.y);
    result.min.z = fminf(b1.min.z, b2.min.z);

    result.max.x = fmaxf(b1.max.x, b2.max.x);
    result.max.y = fmaxf(b1.max.y, b2.max.y);
    result.max.z = fmaxf(b1.max.z, b2.max.z);
    return result;
}

__device__ __host__
inline BoundingBox BoundingBox::minmax(const VBOVertex& p1, const VBOVertex& p2)
{
    BoundingBox result;

    result.min.x = fminf(p1.x, p2.x);
    result.min.y = fminf(p1.y, p2.y);
    result.min.z = fminf(p1.z, p2.z);

    result.max.x = fmaxf(p1.x, p2.x);
    result.max.y = fmaxf(p1.y, p2.y);
    result.max.z = fmaxf(p1.z, p2.z);
    return result;
}

template <unsigned int blockSize>
__global__ void Cuda_KDTreeFindMinMax(const VBOVertex* tree, const int treeSize, BoundingBox* minmax)
{
    extern __shared__ BoundingBox sdata[];

    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*(blockSize*2) + tid;
    unsigned int gridSize = blockSize*2*gridDim.x;

    while (i < treeSize)
    {
        sdata[tid] = BoundingBox::minmax(tree[i],tree[i+blockSize]);
        //sdata[tid+1] =
        i += gridSize;
        __syncthreads();
    }
    //__syncthreads();

    if (blockSize >= 512) { if (tid < 256) { sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 256]); } __syncthreads(); }
    if (blockSize >= 256) { if (tid < 128) { sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 128]);  } __syncthreads(); }
    if (blockSize >= 128) { if (tid < 64) { sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 64]);  } __syncthreads(); }
    if (tid < 32)
    {
        if (blockSize >= 64) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 32]);
        if (blockSize >= 32) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 16]);
        if (blockSize >= 16) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 8]);
        if (blockSize >= 8) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 4]);
        if (blockSize >= 4) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 2]);
        if (blockSize >= 2) sdata[tid] = BoundingBox::minmax(sdata[tid], sdata[tid + 1]);
    }

    //if (tid == 0) minmax[blockIdx.x] = sdata[0];
}

__global__
void Cuda_KDTreeComputeOctcodes(const VBOVertex* tree, const int treeSize, const BoundingBox bb, OCTCODE* octcodes, int* index)
{
    // get index of thread in grid
    int idx = blockDim.x * blockIdx.x + threadIdx.x;

    // check if thread is valid
    if (idx >= treeSize) return;

    octcodes[idx] = zOrder(tree[idx], bb.min, bb.max);
    index[idx] = idx;
}

__global__ void Cuda_KDTreeOctcodesSortStep(OCTCODE* octcodes, int *index, int j, int k, int size)
{
  unsigned int i, ixj; /* Sorting partners: i and ixj */
  i = threadIdx.x + blockDim.x * blockIdx.x;
  ixj = i^j;

  if (i >= size || ixj >= size) return;

  /* The threads with the lowest ids sort the array. */
  if ((ixj)>i) {
    if ((i&k)==0) {
      /* Sort ascending */
      if (octcodes[i]>octcodes[ixj]) {
        /* exchange(i,ixj); */
        OCTCODE tempo = octcodes[i];
        octcodes[i] = octcodes[ixj];
        octcodes[ixj] = tempo;

        OCTCODE tempi = index[i];
        index[i] = index[ixj];
        index[ixj] = tempi;
      }
    }
    if ((i&k)!=0) {
      /* Sort descending */
      if (octcodes[i]<octcodes[ixj]) {
        /* exchange(i,ixj); */
          OCTCODE tempo = octcodes[i];
          octcodes[i] = octcodes[ixj];
          octcodes[ixj] = tempo;

          OCTCODE tempi = index[i];
          index[i] = index[ixj];
          index[ixj] = tempi;
      }
    }
  }
}

void C_KDTreeFindMinMax(const KernelRunData& data, const VBOVertex* tree, const int treeSize, BoundingBox& minmax)
{
    /*
    BoundingBox* c_results;
    cudaMalloc(&c_results, sizeof(BoundingBox) * data.dimGrid.x);
    gpuErrchk();

    unsigned int sharedmemorysize = data.dimBlock.x * sizeof(BoundingBox);
    switch (data.dimBlock.x)
    {
    case 512:
        Cuda_KDTreeFindMinMax<512><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 256:
        Cuda_KDTreeFindMinMax<256><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 128:
        Cuda_KDTreeFindMinMax<128><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 64:
        Cuda_KDTreeFindMinMax<64><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 32:
        Cuda_KDTreeFindMinMax<32><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 16:
        Cuda_KDTreeFindMinMax<16><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 8:
        Cuda_KDTreeFindMinMax<8><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 4:
        Cuda_KDTreeFindMinMax<4><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 2:
        Cuda_KDTreeFindMinMax<2><<<data.dimGrid, data.dimGrid, sharedmemorysize>>>(tree, treeSize, c_results); break;
    case 1:
        Cuda_KDTreeFindMinMax<1><<<data.dimGrid, data.dimGrid>>>(tree, treeSize, minmax); break;
    }
    gpuErrchk();

    cudaFree(c_results);
    */

    std::vector<VBOVertex> cpu_tree;
    cpu_tree.resize(treeSize);

    cudaMemcpy(&cpu_tree[0], tree, sizeof(VBOVertex) * treeSize, cudaMemcpyDeviceToHost);
    gpuErrchk();

    minmax.min = cpu_tree[0];
    minmax.max = cpu_tree[0];

    for (int i = 0; i < treeSize; ++i)
    {
        minmax.expand(cpu_tree[i]);
    }
}

void C_KDTreeComputeOctcodes(const KernelRunData& data, const VBOVertex* tree, const int treeSize, const BoundingBox& bb, OCTCODE* octcodes, int* index)
{
    Cuda_KDTreeComputeOctcodes<<<data.dimGrid, data.dimBlock>>>(tree, treeSize, bb, octcodes, index);
}

void C_KDTreeOctcodesSort(const KernelRunData& data, OCTCODE* octcodes, int* index, int size)
{
    int j, k;
    /* Major step */
    for (k = 2; k <= size; k <<= 1) {
      /* Minor step */
      for (j=k>>1; j>0; j=j>>1) {
        Cuda_KDTreeOctcodesSortStep<<<data.dimGrid, data.dimBlock>>>(octcodes, index, j, k, size);
      }
    }
}
