#ifndef CUDASCOPEDTIMER_H
#define CUDASCOPEDTIMER_H

#include <cuda_runtime.h>

class CudaScopedTimer
{
public:
    CudaScopedTimer(const char* kernelName);
    ~CudaScopedTimer();
private:
    cudaEvent_t mStart;
    cudaEvent_t mStop;
    const char* mKernelName;
};

#endif // CUDASCOPEDTIMER_H
