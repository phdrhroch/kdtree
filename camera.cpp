#include "stdafx.h"

#include "camera.h"

#include <GL/gl.h>

#include <QDebug>

static void loadMatrix(const QMatrix4x4& m)
{
    // static to prevent glLoadMatrixf to fail on certain drivers
    glLoadMatrixd(m.constData());
}

Camera::Camera(const QVector3D& eye, const QVector3D& center, const QVector3D& up, float fov, int w, int h):
    mFov(fov),
    mW(w),
    mH(h)
{
    mModelView.lookAt(eye, center, up);

    float aspect = float(w) / float(h);
    mProjection.perspective(fov, aspect, 0.1f, 1500.0f);
    mTrackBall = new TrackBall(TrackBall::Plane);
    //qDebug() << mModelView;
    //qDebug() << mProjection;
}

void Camera::apply()
{
    // apply camera
    glMatrixMode(GL_PROJECTION);
    loadMatrix(mProjection);
    glLoadMatrixd(mProjection.constData());

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(mModelView.constData());

    glViewport(0, 0, mW, mH);
}

void Camera::resize(int w, int h)
{
    float aspect = float(w) / float(h);
    mProjection.setToIdentity();
    mProjection.perspective(mFov, aspect, 0.1f, 90.0f);
    mW = w;
    mH = h;
}

void Camera::zoom(int delta)
{
    if (delta < 0)
    {
        QMatrix4x4 translation; translation.translate(QVector3D(0, 0, -1.0f));
        mModelView = translation * mModelView;
    }
    else
    {
        QMatrix4x4 translation; translation.translate(QVector3D(0, 0, 1.0f));
        mModelView = translation * mModelView;
    }
}

void Camera::movespush(int x, int y)
{
    mMoveCoords = QVector2D(x, y);
}

void Camera::moverelease(int x, int y)
{
    float normalized_x = 2.0f * float(x - mMoveCoords.x()) / float(mW);
    float normalized_y = -2.0f * float(y - mMoveCoords.y()) / float(mH);

    QMatrix4x4 translation; translation.translate(normalized_x, normalized_y, 0);
    mModelView = translation * mModelView;

    mMoveCoords = QVector2D(x, y);
}

void Camera::arcballpush(int x, int y)
{
    float normalized_x = 2.0 * float(x) / float(mW) - 1.0;
    float normalized_y = 1.0 - 2.0 * float(y) / float(mH);
    mTrackBall->push(QPointF(normalized_x, normalized_y), QQuaternion());
}

void Camera::arcballrelease(int x, int y)
{
    float normalized_x = 2.0 * float(x) / float(mW) - 1.0;
    float normalized_y = 1.0 - 2.0 * float(y) / float(mH);
    mTrackBall->release(QPointF(normalized_x,  normalized_y), QQuaternion());
    mTrackBall->push(QPointF(normalized_x,  normalized_y), QQuaternion());
    //mModelView.setToIdentity();

    QMatrix4x4 rotation; rotation.rotate(mTrackBall->rotation());
    mModelView = mModelView * rotation;
}
